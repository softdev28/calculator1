import 'package:calculator/calculator.dart' as calculator;
import 'dart:io';

class calculate {
  double sum(double x, double y) {
    return x + y;
  }

  double subtract(double x, double y) {
    return x - y;
  }

  double multiply(double x, double y) {
    return x * y;
  }

  double divide(double x, double y) {
    return x / y;
  }
}

void main(List<String> arguments) {
  calculate cal = calculate();
  print("1.Sum");
  print("2.subtract");
  print("3.multiply");
  print("4.divide");
  print("5.Exit");

  String? input = stdin.readLineSync();

  if (input == "5") {
    print("Exit programe. ");
    exit(0);
  } else {
    print("Enter First number:  ");
    String? firstInput = stdin.readLineSync();
    double x = double.parse(firstInput!);
    print("Enter Second number: ");
    String? secondInput = stdin.readLineSync();
    double y = double.parse(secondInput!);
    double result;
    switch (input) {
      case "1":
        result = cal.sum(x, y);
        print("$x + $y = $result");
        break;
      case "2":
        result = cal.subtract(x, y);
        print("$x - $y = $result");
        break;
      case "3":
        result = cal.multiply(x, y);
        print("$x * $y = $result");
        break;
      case "4":
        result = cal.divide(x, y);
        print("$x / $y = $result");
        break;
    }

  }
}
